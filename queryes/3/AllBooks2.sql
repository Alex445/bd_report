﻿-- DROP TABLE Книги;
-- DROP TABLE Издательства;
-- DROP TABLE Города;
-- DROP TABLE Авторы;

----------------------------------------------
-- Авторы
----------------------------------------------

CREATE TABLE Авторы
(
  КодАвтора integer NOT NULL,
  ФИО varchar(50) NOT NULL
);

ALTER TABLE Авторы
ADD CONSTRAINT pk_Авторы PRIMARY KEY(КодАвтора);

INSERT INTO Авторы VALUES (1,'Гребенюк Е.И.');
INSERT INTO Авторы VALUES (2,'Михеева Е.В.');
INSERT INTO Авторы VALUES (3,'Голенищев Э.П.');
INSERT INTO Авторы VALUES (4,'Кузовкин А.В.');
INSERT INTO Авторы VALUES (5,'Уткин В.Б.');
INSERT INTO Авторы VALUES (6,'Могилев А.В.');
INSERT INTO Авторы VALUES (7,'Моосмюллер Г.');
INSERT INTO Авторы VALUES (8,'Филимонова Е.В.');
INSERT INTO Авторы VALUES (9,'Харуто А.В.');
INSERT INTO Авторы VALUES (10,'Алешин Л.И.');
INSERT INTO Авторы VALUES (11,'Избачков Ю.С.');
INSERT INTO Авторы VALUES (12,'Сергеев А.В.');
INSERT INTO Авторы VALUES (13,'Архангельский А.Я.');
INSERT INTO Авторы VALUES (14,'Бобровский С.И.');
INSERT INTO Авторы VALUES (15,'Браун С.');
INSERT INTO Авторы VALUES (16,'Ивасенко А.Г.');
INSERT INTO Авторы VALUES (17,'Макарова Н.В.');
INSERT INTO Авторы VALUES (18,'Малыхина М.П.');
INSERT INTO Авторы VALUES (19,'Михелёв В.М.');
INSERT INTO Авторы VALUES (20,'Муромцев В.В.');
INSERT INTO Авторы VALUES (21,'Романов А.Н.');
INSERT INTO Авторы VALUES (22,'Степанов А.Н.');
INSERT INTO Авторы VALUES (23,'Фаронов В.В.');
INSERT INTO Авторы VALUES (24,'Балдин К.В.');
INSERT INTO Авторы VALUES (25,'Гобарева Я.Л.');
INSERT INTO Авторы VALUES (26,'Исаев Г.Н.');
INSERT INTO Авторы VALUES (27,'Культин Н.Б.');
INSERT INTO Авторы VALUES (28,'Фуфаев Э.В.');
INSERT INTO Авторы VALUES (29,'Бекаревич Ю.Б.');
INSERT INTO Авторы VALUES (30,'Бондаренко С.В.');
INSERT INTO Авторы VALUES (31,'Келим Ю.М.');
INSERT INTO Авторы VALUES (32,'Кузин А.В.');
INSERT INTO Авторы VALUES (33,'Кузнецов И.Н.');
INSERT INTO Авторы VALUES (34,'Попов В.Б.');
INSERT INTO Авторы VALUES (35,'Сагиян С.');
INSERT INTO Авторы VALUES (36,'Советов Б.Я.');
INSERT INTO Авторы VALUES (37,'Сорокин А.В.');
INSERT INTO Авторы VALUES (38,'Долженков В.А.');
INSERT INTO Авторы VALUES (39,'Золотова С.И.');
INSERT INTO Авторы VALUES (40,'Зубов А.В.');
INSERT INTO Авторы VALUES (41,'Симонович С.В.');
INSERT INTO Авторы VALUES (42,'Евсеев Г.А.');
INSERT INTO Авторы VALUES (43,'Фигурнов В.Э.');
INSERT INTO Авторы VALUES (44,'Алексеев А.Г.');
INSERT INTO Авторы VALUES (45,'Денисов А.');
INSERT INTO Авторы VALUES (46,'Вихарев И.');
INSERT INTO Авторы VALUES (47,'Белов А.');
INSERT INTO Авторы VALUES (48,'Шкаев А.В.');
INSERT INTO Авторы VALUES (49,'Брябрин В.М.');
INSERT INTO Авторы VALUES (50,'Комягин В.Б.');
INSERT INTO Авторы VALUES (51,'Коцюбинский А.О.');
INSERT INTO Авторы VALUES (52,'Фролов А.В.');
INSERT INTO Авторы VALUES (53,'Фролов Г.В.');
INSERT INTO Авторы VALUES (54,'Грошев С.В.');
INSERT INTO Авторы VALUES (55,'Гончаров А.');
INSERT INTO Авторы VALUES (56,'Абрамов С.А.');
INSERT INTO Авторы VALUES (57,'Зима Е.В.');
INSERT INTO Авторы VALUES (58,'Острейковский В.А.');
INSERT INTO Авторы VALUES (59,'Шафрин Ю.А.');
INSERT INTO Авторы VALUES (60,'Поспелов Д.А.');
INSERT INTO Авторы VALUES (61,'Ахо А.');
INSERT INTO Авторы VALUES (62,'Хопкрофт Дж.');
INSERT INTO Авторы VALUES (63,'Ульман Дж.');
INSERT INTO Авторы VALUES (64,'Ершов А.П.');
INSERT INTO Авторы VALUES (65,'Кнут Д.');
INSERT INTO Авторы VALUES (66,'Новиков Ф.А.');
INSERT INTO Авторы VALUES (67,'Фихтенгольц Г.М.');
INSERT INTO Авторы VALUES (68,'Кураков Л.П.');
INSERT INTO Авторы VALUES (69,'Лебедев Е.К.');
INSERT INTO Авторы VALUES (70,'Пак Н.И.');
INSERT INTO Авторы VALUES (71,'Хеннер Е.К.');
INSERT INTO Авторы VALUES (72,'Першиков В.И.');
INSERT INTO Авторы VALUES (73,'Савинков В.М.');
INSERT INTO Авторы VALUES (74,'Якубайтис Э.А.');
INSERT INTO Авторы VALUES (75,'Геворкян Г.Х.');
INSERT INTO Авторы VALUES (76,'Семенов В.Н.');
INSERT INTO Авторы VALUES (77,'Кетков Ю.Л.');
INSERT INTO Авторы VALUES (78,'Якушева Н.М.');
INSERT INTO Авторы VALUES (79,'Араманович И.Г.');
INSERT INTO Авторы VALUES (80,'Лунц Г.Л.');
INSERT INTO Авторы VALUES (81,'Эльсгольц А.Э.');
INSERT INTO Авторы VALUES (82,'Бугров Я.С.');
INSERT INTO Авторы VALUES (83,'Никольский С.М.');
INSERT INTO Авторы VALUES (84,'Жевержиев В.Ф.');
INSERT INTO Авторы VALUES (85,'Кальницкий Л.А.');
INSERT INTO Авторы VALUES (86,'Сапогов Н.А.');
INSERT INTO Авторы VALUES (87,'Лаврентьев М.А.');
INSERT INTO Авторы VALUES (88,'Шабат Б.В.');
INSERT INTO Авторы VALUES (89,'Маркушевич А.И.');
INSERT INTO Авторы VALUES (90,'Привалов И.И.');
INSERT INTO Авторы VALUES (91,'Сидоров Ю.В.');
INSERT INTO Авторы VALUES (92,'Федорюк М.В.');
INSERT INTO Авторы VALUES (93,'Шабунин М.И.');
INSERT INTO Авторы VALUES (94,'Воробьев Н.Н.');
INSERT INTO Авторы VALUES (95,'Толстов Г.П.');
INSERT INTO Авторы VALUES (96,'Харди Г.Х.');
INSERT INTO Авторы VALUES (97,'Рогозинский В.В.');
INSERT INTO Авторы VALUES (98,'Шмелев П.А.');
INSERT INTO Авторы VALUES (99,'Дёч Г.');
INSERT INTO Авторы VALUES (100,'Диткин В.А.');
INSERT INTO Авторы VALUES (101,'Кузнецов П.И.');
INSERT INTO Авторы VALUES (102,'Прудников А.П.');
INSERT INTO Авторы VALUES (103,'Мышкис А.Д.');
INSERT INTO Авторы VALUES (104,'Эфрос А.М.');
INSERT INTO Авторы VALUES (105,'Данилевский А.М.');
INSERT INTO Авторы VALUES (106,'Вихман Э.');
INSERT INTO Авторы VALUES (107,'Волькенштейн В.С.');
INSERT INTO Авторы VALUES (108,'Детлаф А.А.');
INSERT INTO Авторы VALUES (109,'Яворский Б.М.');
INSERT INTO Авторы VALUES (110,'Жукарев А.С.');
INSERT INTO Авторы VALUES (111,'Матвеев А.Н.');
INSERT INTO Авторы VALUES (112,'Петерсон В.К.');
INSERT INTO Авторы VALUES (113,'Иродов И.Е.');
INSERT INTO Авторы VALUES (114,'Калашников С.Г.');
INSERT INTO Авторы VALUES (115,'Китель И.');
INSERT INTO Авторы VALUES (116,'Найт У.');
INSERT INTO Авторы VALUES (117,'Рудерман М.');
INSERT INTO Авторы VALUES (118,'Парселл Э.');
INSERT INTO Авторы VALUES (119,'Рейф Ф.');
INSERT INTO Авторы VALUES (120,'Трофимова Т.И.');
INSERT INTO Авторы VALUES (121,'Хайкин С.Э.');
INSERT INTO Авторы VALUES (122,'Пинский А.А.');

----------------------------------------------
-- Города
----------------------------------------------

CREATE TABLE Города
(
  КодГорода varchar(20) NOT NULL,
  Название varchar(50) NOT NULL
);

ALTER TABLE Города
ADD CONSTRAINT pk_Города PRIMARY KEY(КодГорода);

INSERT INTO Города VALUES ('М.','Москва');
INSERT INTO Города VALUES ('СПб.','Санкт-Петербург');
INSERT INTO Города VALUES ('Ростов н/Д','Ростов на Дону');
INSERT INTO Города VALUES ('Белгород','Белгород');
INSERT INTO Города VALUES ('Харьков','Харьков');

----------------------------------------------
-- Издательства
----------------------------------------------

CREATE TABLE Издательства
(
  КодИздат integer NOT NULL,
  Название varchar(50) NOT NULL,
  КодГорода varchar(20)
);

ALTER TABLE Издательства
ADD CONSTRAINT pk_Издательства PRIMARY KEY(КодИздат);

ALTER TABLE Издательства
ADD CONSTRAINT fk_Города FOREIGN KEY(КодГорода) REFERENCES Города(КодГорода);

INSERT INTO Издательства VALUES (1,'Академия','М.');
INSERT INTO Издательства VALUES (2,'Феникс','Ростов н/Д');
INSERT INTO Издательства VALUES (3,'ИНФРА-М','М.');
INSERT INTO Издательства VALUES (4,'ЛКИ','М.');
INSERT INTO Издательства VALUES (5,'Литера','М.');
INSERT INTO Издательства VALUES (6,'Питер','СПб.');
INSERT INTO Издательства VALUES (7,'Бином-Пресс','М.');
INSERT INTO Издательства VALUES (8,'КноРус','М.');
INSERT INTO Издательства VALUES (9,'БХВ-Петербург','СПб.');
INSERT INTO Издательства VALUES (10,'БелГУ','Белгород');
INSERT INTO Издательства VALUES (11,'Вузовский учебник','М.');
INSERT INTO Издательства VALUES (12,'Дашков и К','М.');
INSERT INTO Издательства VALUES (13,'Омега-Л','М.');
INSERT INTO Издательства VALUES (14,'Финансы и статистика','М.');
INSERT INTO Издательства VALUES (15,'Высшая школа','М.');
INSERT INTO Издательства VALUES (16,'ЮНИТИ-ДАНА','М.');
INSERT INTO Издательства VALUES (17,'АСТпресс','М.');
INSERT INTO Издательства VALUES (18,'Инфра-М','М.');
INSERT INTO Издательства VALUES (19,'Радио и связь','М.');
INSERT INTO Издательства VALUES (20,'Наука','М.');
INSERT INTO Издательства VALUES (21,'Нолидж','М.');
INSERT INTO Издательства VALUES (22,'Диалог-МИФИ','М.');
INSERT INTO Издательства VALUES (23,'Триумф','М.');
INSERT INTO Издательства VALUES (24,'АБВ','М.');
INSERT INTO Издательства VALUES (25,'Педагогика-Пресс','М.');
INSERT INTO Издательства VALUES (26,'Бином','М.');
INSERT INTO Издательства VALUES (27,'Мир','М.');
INSERT INTO Издательства VALUES (28,'Вуз и школа','М.');
INSERT INTO Издательства VALUES (29,'ГроссМедиа','М.');
INSERT INTO Издательства VALUES (30,'Физматгиз','М.');
INSERT INTO Издательства VALUES (31,'ОНТИ','Харьков');
INSERT INTO Издательства VALUES (32,'Эдиториал УРСС','М.');
INSERT INTO Издательства VALUES (33,'Лаборатория базовых знаний','М.');
INSERT INTO Издательства VALUES (34,'ФИЗМАТЛИТ','М.');

----------------------------------------------
-- Книги
----------------------------------------------

CREATE TABLE Книги
(
  КодКниги integer NOT NULL,
  КодыАвторов integer[],
  Название varchar(200) NOT NULL,
  КодИздат integer,
  Год integer
);

ALTER TABLE Книги
ADD CONSTRAINT pk_Книги PRIMARY KEY(КодКниги);

ALTER TABLE Книги
ADD CONSTRAINT fk_Издательства FOREIGN KEY(КодИздат) REFERENCES Издательства(КодИздат);

INSERT INTO Книги VALUES (1,ARRAY[1],'Технические средства информатизации',1,2011);
INSERT INTO Книги VALUES (2,ARRAY[2],'Информационные технологии в профессиональной деятельности',1,2011);
INSERT INTO Книги VALUES (3,ARRAY[3],'Информационное обеспечение систем управления',2,2010);
INSERT INTO Книги VALUES (4,ARRAY[4],'Управление данными',1,2010);
INSERT INTO Книги VALUES (5,ARRAY[5],'Информационные системы в экономике',1,2010);
INSERT INTO Книги VALUES (6,ARRAY[6],'Информатика',1,2009);
INSERT INTO Книги VALUES (7,ARRAY[7],'Маркетинговые исследования с SPSS',3,2009);
INSERT INTO Книги VALUES (8,ARRAY[8],'Информационные технологии в профессиональной деятельности',2,2009);
INSERT INTO Книги VALUES (9,ARRAY[9],'Музыкальная информатика',4,2009);
INSERT INTO Книги VALUES (10,ARRAY[10],'Информационные технологии',5,2008);
INSERT INTO Книги VALUES (11,ARRAY[11],'Информационные системы',6,2008);
INSERT INTO Книги VALUES (12,ARRAY[12],'Access 2007. Новые возможности',6,2008);
INSERT INTO Книги VALUES (13,ARRAY[13],'Программирование в Delphi для Windows',7,2007);
INSERT INTO Книги VALUES (14,ARRAY[14],'Технологии С#Builder. Разработка приложений для бизнеса',6,2007);
INSERT INTO Книги VALUES (15,ARRAY[15],'Visual Basic 6',6,2007);
INSERT INTO Книги VALUES (16,ARRAY[16],'Информационные технологии в экономике и управлении',8,2007);
INSERT INTO Книги VALUES (17,ARRAY[17],'Компьютерное делопроизводство. Учебный курс',6,2007);
INSERT INTO Книги VALUES (18,ARRAY[18],'Базы данных: основы, проектирование, использование',9,2007);
INSERT INTO Книги VALUES (19,ARRAY[19],'Базы данных и СУБД',10,2007);
INSERT INTO Книги VALUES (20,ARRAY[20],'Проектирование информационных систем',10,2007);
INSERT INTO Книги VALUES (21,ARRAY[21],'Информационные системы в экономике (лекции, упражнения и задачи)',11,2007);
INSERT INTO Книги VALUES (22,ARRAY[22],'Информатика',6,2007);
INSERT INTO Книги VALUES (23,ARRAY[23],'Delphi 2005. Язык, среда, разработка приложений',6,2007);
INSERT INTO Книги VALUES (24,ARRAY[23],'Turbo Pascal',6,2007);
INSERT INTO Книги VALUES (25,ARRAY[24],'Информационные системы в экономике',12,2006);
INSERT INTO Книги VALUES (26,ARRAY[25],'Технология экономических расчетов средствами MS Exsel',8,2006);
INSERT INTO Книги VALUES (27,ARRAY[11],'Информационные системы',6,2006);
INSERT INTO Книги VALUES (28,ARRAY[26],'Информационные системы в экономике',13,2006);
INSERT INTO Книги VALUES (29,ARRAY[27],'Самоучитель С++ Builder',9,2006);
INSERT INTO Книги VALUES (30,ARRAY[23],'Delphi 2005',6,2006);
INSERT INTO Книги VALUES (31,ARRAY[23],'Delphi 2005. Разработка приложений для баз данных и Интернета',6,2006);
INSERT INTO Книги VALUES (32,ARRAY[23],'Программирование баз данных в Delphi 7',6,2006);
INSERT INTO Книги VALUES (33,ARRAY[28],'Пакеты прикладных программ',1,2006);
INSERT INTO Книги VALUES (34,ARRAY[29],'Microsoft Access за 21 занятие для студента',9,2005);
INSERT INTO Книги VALUES (35,ARRAY[30],'Самое главное о... Microsoft Office',6,2005);
INSERT INTO Книги VALUES (36,ARRAY[31],'Вычислительная техника',1,2005);
INSERT INTO Книги VALUES (37,ARRAY[32],'Базы данных',1,2005);
INSERT INTO Книги VALUES (38,ARRAY[33],'Интернет в учебной и научной работе',12,2005);
INSERT INTO Книги VALUES (39,ARRAY[27],'Visual Basic. Освой самостоятельно',9,2005);
INSERT INTO Книги VALUES (40,ARRAY[2],'Практикум по информационным технологиям в профессиональной деятельности',1,2005);
INSERT INTO Книги VALUES (41,ARRAY[34],'Основы информационных и телекоммуникационнных технологий',14,2005);
INSERT INTO Книги VALUES (42,ARRAY[35],'Делопроизводство на компьютере',6,2005);
INSERT INTO Книги VALUES (43,ARRAY[36],'Базы данных: теория и практика',15,2005);
INSERT INTO Книги VALUES (44,ARRAY[37],'Delphi. Разработка баз данных',6,2005);
INSERT INTO Книги VALUES (45,ARRAY[5],'Информационные системы и технологии в экономике',16,2005);
INSERT INTO Книги VALUES (46,ARRAY[28],'Базы данных',1,2005);
INSERT INTO Книги VALUES (47,ARRAY[38],'Microsoft Excel 2003',9,2004);
INSERT INTO Книги VALUES (48,ARRAY[39],'Практикум по Access',14,2004);
INSERT INTO Книги VALUES (49,ARRAY[40],'Информационные технологии в лингвистике',1,2004);
INSERT INTO Книги VALUES (50,ARRAY[32],'Микропроцессорная техника',1,2004);
INSERT INTO Книги VALUES (51,ARRAY[27],'Visual Basic. Освой на примерах',9,2004);
INSERT INTO Книги VALUES (52,ARRAY[41,42],'Практическая информатика. Учебное пособие',17,1999);
INSERT INTO Книги VALUES (53,ARRAY[43],'IBM PC для пользователя',18,2001);
INSERT INTO Книги VALUES (54,ARRAY[41,42,44],'Специальная информатика. Учебное пособие',17,1999);
INSERT INTO Книги VALUES (55,ARRAY[45,46,47],'Самоучитель Интернет',6,2001);
INSERT INTO Книги VALUES (56,ARRAY[48],'Руководство по работе на персональном компьютере. Справочник',19,1994);
INSERT INTO Книги VALUES (57,ARRAY[49],'Программное обеспечение персональных ЭВМ',20,1990);
INSERT INTO Книги VALUES (58,ARRAY[50,51],'Excel 7.0 в примерах',21,1996);
INSERT INTO Книги VALUES (59,ARRAY[52,53],'Глобальные сети компьютеров. Практическое введение в Internet, E-Mail, FTP, WWW и HTML',22,1996);
INSERT INTO Книги VALUES (60,ARRAY[51,54],'Современный самоучитель работы в сети Интернет',23,1997);
INSERT INTO Книги VALUES (61,ARRAY[55],'HTML в примерах',6,1997);
INSERT INTO Книги VALUES (62,ARRAY[55],'Excel 7.0 в примерах',6,1996);
INSERT INTO Книги VALUES (63,ARRAY[56,57],'Начала информатики',20,1989);
INSERT INTO Книги VALUES (64,ARRAY[58],'Информатика',15,2007);
INSERT INTO Книги VALUES (65,ARRAY[59],'Основы компьютерной технологии',24,1997);
INSERT INTO Книги VALUES (66,ARRAY[60],'Информатика: Энциклопедический словарь для начинающих',25,1994);
INSERT INTO Книги VALUES (67,ARRAY[13],'Программирование в Delphi 5. 2-е изд., перераб. и доп.',26,2000);
INSERT INTO Книги VALUES (68,ARRAY[61,62,63],'Построение и анализ вычислительных алгоритмов',27,1979);
INSERT INTO Книги VALUES (69,ARRAY[64],'Введение в теоретическое программирование',20,1977);
INSERT INTO Книги VALUES (70,ARRAY[65],'Искусство программирования для ЭВМ, т.1',27,1977);
INSERT INTO Книги VALUES (71,ARRAY[65],'Искусство программирования для ЭВМ, т.2',27,1977);
INSERT INTO Книги VALUES (72,ARRAY[65],'Искусство программирования для ЭВМ, т.3',27,1977);
INSERT INTO Книги VALUES (73,ARRAY[66],'Дискретная математика для программистов',6,2000);
INSERT INTO Книги VALUES (74,ARRAY[67],'Курс дифференциального и интегрального исчисления, т.1',20,1966);
INSERT INTO Книги VALUES (75,ARRAY[67],'Курс дифференциального и интегрального исчисления, т.2',20,1966);
INSERT INTO Книги VALUES (76,ARRAY[68,69],'Информатика',28,2009);
INSERT INTO Книги VALUES (77,ARRAY[6,70,71],'Информатика: Учебное пособие для вузов',1,2008);
INSERT INTO Книги VALUES (78,ARRAY[72,73],'Толковый словарь по информатике, 2-е изд.',14,2008);
INSERT INTO Книги VALUES (79,ARRAY[74],'Информационные сети и системы: Справочная книга',14,2008);
INSERT INTO Книги VALUES (80,ARRAY[15],'Visual Basic 6',6,2001);
INSERT INTO Книги VALUES (81,ARRAY[75,76],'Бейсик - это просто',19,1989);
INSERT INTO Книги VALUES (82,ARRAY[77],'Диалог на языке бейсик для мини- и микро-ЭВМ',20,1988);
INSERT INTO Книги VALUES (83,ARRAY[51,54],'Excel для бухгалтера в примерах',29,2004);
INSERT INTO Книги VALUES (84,ARRAY[78],'Visual Basic для студентов',19,2001);
INSERT INTO Книги VALUES (85,ARRAY[79,80,81],'Функции комплексного переменного. Операционное исчисление. Теория устойчивости',20,1965);
INSERT INTO Книги VALUES (86,ARRAY[82,83],'Высшая математика. Дифференциальные уравнения. Кратные интегралы. Ряды. Функции комплексного переменного. Учеб. для втузов',20,1985);
INSERT INTO Книги VALUES (87,ARRAY[84,85,86],'Специальный курс высшей математики для втузов',15,1970);
INSERT INTO Книги VALUES (88,ARRAY[87,88],'Методы теории функций комплексного переменного: Учебное пособие',20,1987);
INSERT INTO Книги VALUES (89,ARRAY[89],'Краткий курс теории аналитических функций',20,1978);
INSERT INTO Книги VALUES (90,ARRAY[90],'Введение в теорию функций комплексного переменного',20,1967);
INSERT INTO Книги VALUES (91,ARRAY[91,92,93],'Лекции по теории функций комплексного переменного',20,1989);
INSERT INTO Книги VALUES (92,ARRAY[88],'Введение в комплексный анализ. Ч.1. Функции одного переменного',20,1985);
INSERT INTO Книги VALUES (93,ARRAY[94],'Теория рядов',20,1970);
INSERT INTO Книги VALUES (94,ARRAY[95],'Ряды Фурье',20,1980);
INSERT INTO Книги VALUES (95,ARRAY[96,97],'Ряды Фурье',30,1962);
INSERT INTO Книги VALUES (96,ARRAY[98],'Теория рядов в задачах и упражнениях: Учебное пособие для студентов вузов',15,1983);
INSERT INTO Книги VALUES (97,ARRAY[99],'Руководство к практическому применению преобразования Лапласа',20,1971);
INSERT INTO Книги VALUES (98,ARRAY[100,101],'Справочник по операционному исчислению',15,1965);
INSERT INTO Книги VALUES (99,ARRAY[100,102],'Интегральное преобразование и операционное исчисление',30,1961);
INSERT INTO Книги VALUES (100,ARRAY[100,102],'Операционное исчисление',15,1966);
INSERT INTO Книги VALUES (101,ARRAY[103],'Математика для втузов. Специальные курсы',20,1971);
INSERT INTO Книги VALUES (102,ARRAY[104,105],'Операционное исчисление и контурные интегралы',31,1937);
INSERT INTO Книги VALUES (103,ARRAY[106],'Берклеевский курс физики. Квантовая физика',20,2001);
INSERT INTO Книги VALUES (104,ARRAY[107],'Сборник задач по общему курсу физики',20,2003);
INSERT INTO Книги VALUES (105,ARRAY[108,109],'Курс общей физики',15,1989);
INSERT INTO Книги VALUES (106,ARRAY[110,111,112],'Задачи повышенной сложности в курсе общей физики: Учебное пособие. 2-е изд.',32,2001);
INSERT INTO Книги VALUES (107,ARRAY[113],'Задачи по общей физике',26,2004);
INSERT INTO Книги VALUES (108,ARRAY[113],'Механика. Основные законы',33,2001);
INSERT INTO Книги VALUES (109,ARRAY[113],'Электромагнетизм. Основные законы',33,2001);
INSERT INTO Книги VALUES (110,ARRAY[114],'Электричество',20,2005);
INSERT INTO Книги VALUES (111,ARRAY[115,116,117],'Берклеевский курс физики. Механика',20,2003);
INSERT INTO Книги VALUES (112,ARRAY[118],'Берклеевский курс физики. Электричество и магнетизм',20,1983);
INSERT INTO Книги VALUES (113,ARRAY[119],'Берклеевский курс физики. Статистическая физика',20,2009);
INSERT INTO Книги VALUES (114,ARRAY[120],'Краткий курс физики',15,2000);
INSERT INTO Книги VALUES (115,ARRAY[121],'Физические основы механики',20,2003);
INSERT INTO Книги VALUES (116,ARRAY[109,122],'Основы физики, т.1',34,2000);
INSERT INTO Книги VALUES (117,ARRAY[109,122],'Основы физики, т.2',34,2000);
