CREATE OR REPLACE FUNCTION get_array()
Returns int[] AS $$
    Declare A int[];
    N int;

    BEGIN
        N=(10*random()+1)::int;
        FOR I IN 1..N LOOP
            A[I]=(random()*(40+10)-10)::int;
        END LOOP;
    Return A;
    END; 
$$ language plpgSQL;
