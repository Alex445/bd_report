CREATE OR REPLACE FUNCTION insert_data(IN N INTEGER)
Returns void AS $$
    Declare 
    A int[];
    q INT := 0;
    k INT;

    BEGIN
        
        FOR I IN 1..N 
        LOOP
            k=(10*random()+1)::int;
            FOR I IN 1..k 
            LOOP
                A[I]=(random()*(40+10)-10)::int;
            END LOOP;

            INSERT INTO Данные VALUES(I, A);
            A := ARRAY[]::int[];
        END LOOP;
    END; 
$$ language plpgsql;