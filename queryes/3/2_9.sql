--DROP FUNCTION Authors(F character varying(200));
CREATE OR REPLACE FUNCTION Authors(F character varying(200))
 
RETURNS TABLE(Код integer, ФИОАвтора character varying(200), Количество integer, КодКниг int, НазваниеКниг character varying(200)) AS $$
 
BEGIN
RETURN QUERY(SELECT КодАвтора, ФИО, array_length(КодыАвторов, 1), КодКниги, Название
FROM Авторы, Книги
WHERE ФИО = F
AND КодАвтора=ANY(КодыАвторов)
);
END;
$$ language plpgsql;
SELECT * FROM Authors('Могилев А.В.')
