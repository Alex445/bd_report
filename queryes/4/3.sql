CREATE OR REPLACE FUNCTION helper(mas int[], number int) 
RETURNS int AS $$ 

    DECLARE resint = 0;

    BEGIN 
        FOR K IN 1..array_length (mas, 1) LOOP
            IF mas[K] = number THEN 
                res = res + 1;
            END IF;

        END LOOP;

    RETURN res;

    END;

$$ LANGUAGE p l p g s q l;

Select helper(ARRAY [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1], 6);