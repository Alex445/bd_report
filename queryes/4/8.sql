SELECT id, line, regexp_matches(line, ’^[a-f]{11}\d’)
FROM bigtable

SELECT id, line,
regexp_matches(line, ’[a-f]{12}$|^[a-f]{12}\d|\d[a-f]{12}\d’)
FROM bigtable

SELECT id, line, regexp_matches(line, ’[0-9]{25,}’)
FROM bigtable

SELECT id, line, regexp_matches(line, ’(.)\1{5}’)
FROM bigtable
