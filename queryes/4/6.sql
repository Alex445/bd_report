INSERT INTO
    BigTable
SELECT
    id,
    md5(random() :: text)
FROM
    generate_series(1, 100000) AS id;