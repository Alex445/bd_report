CREATE
OR REPLACE FUNCTION f1() RETURNS void AS $ $ DECLARE curs1 CURSOR FOR (
    SELECT
        Очки,
        РазностьМячей,
        Название,
        Группа
    FROM
        Команды
);

p1 int;

p2 int;

p3 int;

p4 int;

r1 int;

r2 int;

r3 int;

r4 int;

name1 text;

name2 text;

name3 text;

name4 text;

gr text;

mas int [];

temp1 int;

flg int = 0;

f1 int;

f2 int;

f3 int;

f4 int;

BEGIN CREATE TABLE Tournament(Название text, Группа text, Очки int);

OPEN curs1;

LOOP FETCH NEXT
FROM
    curs1 INTO p1,
    r1,
    name1,
    gr;

IF NOT FOUND THEN EXIT;

END IF;

FETCH NEXT
FROM
    curs1 INTO p2,
    r2,
    name2,
    gr;

FETCH NEXT
FROM
    curs1 INTO p3,
    r3,
    name3,
    gr;

FETCH NEXT
FROM
    curs1 INTO p4,
    r4,
    name4,
    gr;

mas = ARRAY [p1,p2,p3,p4];

temp1 =(
    SELECT
        max(w)
    FROM
        unnest(mas) As w
);

IF helper(mas, temp1) = 2 THEN IF p1 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name1, gr, p1);

END IF;

IF p2 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name2, gr, p2);

END IF;

IF p3 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name3, gr, p3);

END IF;

IF p4 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name4, gr, p4);

END IF;

CONTINUE;

ELSEIF helper(mas, temp1) = 1 THEN IF p1 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name1, gr, p1);

flg = flg + 1;

END IF;

IF p2 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name2, gr, p2);

flg = flg + 1;

END IF;

IF p3 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name3, gr, p3);

flg = flg + 1;

END IF;

IF p4 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name4, gr, p4);

flg = flg + 1;

END IF;

temp1 = temp1 -1;

LOOP IF p1 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name1, gr, p1);

flg = flg + 1;

f1 = 1;

END IF;

IF p2 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name2, gr, p2);

flg = flg + 1;

f2 = 1;

END IF;

IF p3 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name3, gr, p3);

flg = flg + 1;

f3 = 1;

END IF;

IF p4 = temp1 THEN
INSERT INTO
    Tournament
VALUES
    (name4, gr, p4);

flg = flg + 1;

f4 = 1;

END IF;

temp1 = temp1 -1;

IF flg >= 2 THEN EXIT;

END IF;

END LOOP;

IF flg = 2 THEN CONTINUE;

END IF;

IF flg > 2 THEN IF f1 = f2 THEN IF r1 < r2 THEN
DELETE FROM
    Tournament
WHERE
    Название = name1;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name2;

END IF;

END IF;

IF f1 = f3 THEN IF r1 < r3 THEN
DELETE FROM
    Tournament
WHERE
    Название = name1;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name3;

END IF;

END IF;

IF f1 = f4 THEN IF r1 < r4 THEN
DELETE FROM
    Tournament
WHERE
    Название = name1;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name4;

END IF;

END IF;

IF f2 = f3 THEN IF r2 < r3 THEN
DELETE FROM
    Tournament
WHERE
    Название = name2;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name3;

END IF;

END IF;

IF f2 = f4 THEN IF r2 < r4 THEN
DELETE FROM
    Tournament
WHERE
    Название = name2;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name4;

END IF;

END IF;

IF f3 = f4 THEN IF r3 < r4 THEN
DELETE FROM
    Tournament
WHERE
    Название = name3;

ELSE
DELETE FROM
    Tournament
WHERE
    Название = name4;

END IF;

END IF;

END IF;

END IF;

flg = 0;

f1 = 0;

f2 = 0;

f3 = 0;

f4 = 0;

RAISE NOTICE 'Обработка группы k';

END LOOP;

END;

$ $ LANGUAGE plpgsql;

SELECT
    f1();