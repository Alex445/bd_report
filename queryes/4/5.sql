CREATE TABLE BigTable (
    id integer NOT NULL,
    line text,
    CONSTRAINT pk_BigTable PRIMARY KEY (id)
);