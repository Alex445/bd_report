CREATE OR REPLACE FUNCTION filltable() RETURNS void AS $ $ DECLARE str text;

curs CURSOR FOR (
    SELECT
        s
    FROM
        Данные
);

lin text;

nameofgroup text;

plays text [];

BEGIN OPEN curs;

LOOP FETCH NEXT
FROM
    curs INTO str;

IF NOT FOUND THEN EXIT;

END IF;

lin = substring(
    str
    FROM
        1 FOR 6
);

IF lin = ’ Группа ’ THEN nameofgroup = substring(
    str
    FROM
        8 FOR 1
);

FOR k IN 1..4 LOOP FETCH NEXT
FROM
    curs INTO str;

INSERT INTO
    Команды
VALUES
(str, nameofgroup, 0, 0, 0, 0, 0);

END LOOP;

FOR k IN 1..6 LOOP FETCH NEXT
FROM
    curs INTO str;

plays = regexp_split_to_array(str, ’ [: ] ’);

IF plays [2] :: int > plays [3] :: int THEN
UPDATE
    Команды
SET
    Победы = Победы + 1,
    РазностьМячей = РазностьМячей + plays [2] :: int - plays [3] :: int,
    Очки = Очки + 3 * Победы + Ничьи
WHERE
    Название = plays [1];

UPDATE
    Команды
SET
    Поражения = Поражения + 1,
    РазностьМячей = РазностьМячей + plays [3] :: int - plays [2] :: int,
    Очки = Очки + 3 * Победы + Ничьи
WHERE
    Название = plays [4];

ELSIF plays [2] :: int < plays [3] :: int THEN
UPDATE
    Команды
SET
    Победы = Победы + 1,
    РазностьМячей = РазностьМячей + plays [2] :: int - plays [3] :: int,
    Очки = Очки + 3 * Победы + Ничьи
WHERE
    Название = plays [4];

UPDATE
    Команды
SET
    Поражения = Поражения + 1,
    РазностьМячей = РазностьМячей + plays [3] :: int - plays [2] :: int,
    Очки = Очки + 3 * Победы + Ничьи
WHERE
    Название = plays [1];

ELSE
UPDATE
    Команды
SET
    Ничьи = Ничьи + 1,
    Очки = Очки + 3 * Победы + Ничьи
WHERE
    Название = plays [1]
    OR Название = plays [4];

END IF;

END LOOP;

END IF;

END LOOP;

END;

$ $ LANGUAGE plpgsql;