CREATE OR REPLACE FUNCTION convertint4() RETURNS void AS $$
    DECLARE
    
    rec RECORD;
    tmp_team VARCHAR;

    team1_score INT := 0;
    team2_score INT := 0;
    team3_score INT := 0;
    team4_score INT := 0;

    team1_defeat INT := 0;
    team2_defeat INT := 0;
    team3_defeat INT := 0;
    team4_defeat INT := 0;

    team1_draw INT := 0;
    team2_draw INT := 0;
    team3_draw INT := 0;
    team4_draw INT := 0;

    team1_diff INT := 0;
    team2_diff INT := 0;
    team3_diff INT := 0;
    team4_diff INT := 0;

    BEGIN

        FOR rec IN
            SELECT * FROM Данные
        LOOP
            tmp_team := rec.Матч1[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет1[0] > rec.Счет1[1]);
                team1_defeat := team1_defeat + int4(rec.Счет1[0] < rec.Счет1[1]);
                team1_diff := rec.Счет1[1] - rec.Счет1[0];
                team1_draw := team1_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет1[0] > rec.Счет1[1]);
                team2_defeat := team2_defeat + int4(rec.Счет1[0] < rec.Счет1[1]);
                team2_diff := rec.Счет1[1] - rec.Счет1[0];
                team2_draw := team2_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет1[0] > rec.Счет1[1]);
                team3_defeat := team3_defeat + int4(rec.Счет1[0] < rec.Счет1[1]);
                team3_diff := rec.Счет1[1] - rec.Счет1[0];
                team3_draw := team3_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет1[0] > rec.Счет1[1]);
                team4_defeat := team4_defeat + int4(rec.Счет1[0] < rec.Счет1[1]);
                team4_diff := rec.Счет1[1] - rec.Счет1[0];
                team4_draw := team4_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;

            tmp_team := rec.Матч1[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет1[0] < rec.Счет1[1]);
                team1_defeat := team1_defeat + int4(rec.Счет1[0] > rec.Счет1[1]);
                team1_diff := rec.Счет1[1] - rec.Счет1[0];
                team1_draw := team1_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;  
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет1[0] < rec.Счет1[1]);
                team2_defeat := team2_defeat + int4(rec.Счет1[0] > rec.Счет1[1]);
                team2_diff := rec.Счет1[1] - rec.Счет1[0];
                team2_draw := team2_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет1[0] < rec.Счет1[1]);
                team3_defeat := team3_defeat + int4(rec.Счет1[0] > rec.Счет1[1]);
                team3_diff := rec.Счет1[1] - rec.Счет1[0];
                team3_draw := team3_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет1[0] < rec.Счет1[1]);
                team4_defeat := team4_defeat + int4(rec.Счет1[0] > rec.Счет1[1]);
                team4_diff := rec.Счет1[1] - rec.Счет1[0];
                team4_draw := team4_draw + int4(rec.Счет1[0] = rec.Счет1[1]);
            END IF;

            tmp_team := rec.Матч2[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет2[0] > rec.Счет2[1]);
                team1_defeat := team1_defeat + int4(rec.Счет2[0] < rec.Счет2[1]);
                team1_diff := rec.Счет2[1] - rec.Счет2[0];
                team1_draw := team1_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет2[0] > rec.Счет2[1]);
                team2_defeat := team2_defeat + int4(rec.Счет2[0] < rec.Счет2[1]);
                team2_diff := rec.Счет2[1] - rec.Счет2[0];
                team2_draw := team2_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет2[0] > rec.Счет2[1]);
                team3_defeat := team3_defeat + int4(rec.Счет2[0] < rec.Счет2[1]);
                team3_diff := rec.Счет2[1] - rec.Счет2[0];
                team3_draw := team3_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет2[0] > rec.Счет2[1]);
                team4_defeat := team4_defeat + int4(rec.Счет2[0] < rec.Счет2[1]);
                team4_diff := rec.Счет2[1] - rec.Счет2[0];
                team4_draw := team4_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;

            tmp_team := rec.Матч2[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет2[0] < rec.Счет2[1]);
                team1_defeat := team1_defeat + int4(rec.Счет2[0] > rec.Счет2[1]);
                team1_diff := rec.Счет2[1] - rec.Счет2[0];
                team1_draw := team1_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет2[0] < rec.Счет2[1]);
                team2_defeat := team2_defeat + int4(rec.Счет2[0] > rec.Счет2[1]);
                team2_diff := rec.Счет2[1] - rec.Счет2[0];
                team2_draw := team2_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет2[0] < rec.Счет2[1]);
                team3_defeat := team3_defeat + int4(rec.Счет2[0] > rec.Счет2[1]);
                team3_diff := rec.Счет2[1] - rec.Счет2[0];
                team3_draw := team3_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет2[0] < rec.Счет2[1]);
                team4_defeat := team4_defeat + int4(rec.Счет2[0] > rec.Счет2[1]);
                team4_diff := rec.Счет2[1] - rec.Счет2[0];
                team4_draw := team4_draw + int4(rec.Счет2[0] = rec.Счет2[1]);
            END IF;

            tmp_team := rec.Матч3[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет3[0] > rec.Счет3[1]);
                team1_defeat := team1_defeat + int4(rec.Счет3[0] < rec.Счет3[1]);
                team1_diff := rec.Счет3[1] - rec.Счет3[0];
                team1_draw := team1_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет3[0] > rec.Счет3[1]);
                team2_defeat := team2_defeat + int4(rec.Счет3[0] < rec.Счет3[1]);
                team2_diff := rec.Счет3[1] - rec.Счет3[0];
                team2_draw := team2_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет3[0] > rec.Счет3[1]);
                team3_defeat := team3_defeat + int4(rec.Счет3[0] < rec.Счет3[1]);
                team3_diff := rec.Счет3[1] - rec.Счет3[0];
                team3_draw := team3_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет3[0] > rec.Счет3[1]);
                team4_defeat := team4_defeat + int4(rec.Счет3[0] < rec.Счет3[1]);
                team4_diff := rec.Счет3[1] - rec.Счет3[0];
                team4_draw := team4_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;

            tmp_team := rec.Матч3[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет3[0] < rec.Счет3[1]);
                team1_defeat := team1_defeat + int4(rec.Счет3[0] > rec.Счет3[1]);
                team1_diff := rec.Счет3[1] - rec.Счет3[0];
                team1_draw := team1_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет3[0] < rec.Счет3[1]);
                team2_defeat := team2_defeat + int4(rec.Счет3[0] > rec.Счет3[1]);
                team2_diff := rec.Счет3[1] - rec.Счет3[0];
                team2_draw := team2_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет3[0] < rec.Счет3[1]);
                team3_defeat := team3_defeat + int4(rec.Счет3[0] > rec.Счет3[1]);
                team3_diff := rec.Счет3[1] - rec.Счет3[0];
                team3_draw := team3_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет3[0] < rec.Счет3[1]);
                team4_defeat := team4_defeat + int4(rec.Счет3[0] > rec.Счет3[1]);
                team4_diff := rec.Счет3[1] - rec.Счет3[0];
                team4_draw := team4_draw + int4(rec.Счет3[0] = rec.Счет3[1]);
            END IF;

            tmp_team := rec.Матч4[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет4[0] > rec.Счет4[1]);
                team1_defeat := team1_defeat + int4(rec.Счет4[0] < rec.Счет4[1]);
                team1_diff := rec.Счет4[1] - rec.Счет4[0];
                team1_draw := team1_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет4[0] > rec.Счет4[1]);
                team2_defeat := team2_defeat + int4(rec.Счет4[0] < rec.Счет4[1]);
                team2_diff := rec.Счет4[1] - rec.Счет4[0];
                team2_draw := team2_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет4[0] > rec.Счет4[1]);
                team3_defeat := team3_defeat + int4(rec.Счет4[0] < rec.Счет4[1]);
                team3_diff := rec.Счет4[1] - rec.Счет4[0];
                team3_draw := team3_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет4[0] > rec.Счет4[1]);
                team4_defeat := team4_defeat + int4(rec.Счет4[0] < rec.Счет4[1]);
                team4_diff := rec.Счет4[1] - rec.Счет4[0];
                team4_draw := team4_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;

            tmp_team := rec.Матч4[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет4[0] < rec.Счет4[1]);
                team1_defeat := team1_defeat + int4(rec.Счет4[0] > rec.Счет4[1]);
                team1_diff := rec.Счет4[1] - rec.Счет4[0];
                team1_draw := team1_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет4[0] < rec.Счет4[1]);
                team2_defeat := team2_defeat + int4(rec.Счет4[0] > rec.Счет4[1]);
                team2_diff := rec.Счет4[1] - rec.Счет4[0];
                team2_draw := team2_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет4[0] < rec.Счет4[1]);
                team3_defeat := team3_defeat + int4(rec.Счет4[0] > rec.Счет4[1]);
                team3_diff := rec.Счет4[1] - rec.Счет4[0];
                team3_draw := team3_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет4[0] < rec.Счет4[1]);
                team4_defeat := team4_defeat + int4(rec.Счет4[0] > rec.Счет4[1]);
                team4_diff := rec.Счет4[1] - rec.Счет4[0];
                team4_draw := team4_draw + int4(rec.Счет4[0] = rec.Счет4[1]);
            END IF;

            tmp_team := rec.Матч5[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет5[0] > rec.Счет5[1]);
                team1_defeat := team1_defeat + int4(rec.Счет5[0] < rec.Счет5[1]);
                team1_diff := rec.Счет5[1] - rec.Счет5[0];
                team1_draw := team1_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет5[0] > rec.Счет5[1]);
                team2_defeat := team2_defeat + int4(rec.Счет5[0] < rec.Счет5[1]);
                team2_diff := rec.Счет5[1] - rec.Счет5[0];
                team2_draw := team2_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет5[0] > rec.Счет5[1]);
                team3_defeat := team3_defeat + int4(rec.Счет5[0] < rec.Счет5[1]);
                team3_diff := rec.Счет5[1] - rec.Счет5[0];
                team3_draw := team3_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет5[0] > rec.Счет5[1]);
                team4_defeat := team4_defeat + int4(rec.Счет5[0] < rec.Счет5[1]);
                team4_diff := rec.Счет5[1] - rec.Счет5[0];
                team4_draw := team4_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;

            tmp_team := rec.Матч5[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет5[0] < rec.Счет5[1]);
                team1_defeat := team1_defeat + int4(rec.Счет5[0] > rec.Счет5[1]);
                team1_diff := rec.Счет5[1] - rec.Счет5[0];
                team1_draw := team1_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет5[0] < rec.Счет5[1]);
                team2_defeat := team2_defeat + int4(rec.Счет5[0] > rec.Счет5[1]);
                team2_diff := rec.Счет5[1] - rec.Счет5[0];
                team2_draw := team2_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет5[0] < rec.Счет5[1]);
                team3_defeat := team3_defeat + int4(rec.Счет5[0] > rec.Счет5[1]);
                team3_diff := rec.Счет5[1] - rec.Счет5[0];
                team3_draw := team3_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет5[0] < rec.Счет5[1]);
                team4_defeat := team4_defeat + int4(rec.Счет5[0] > rec.Счет5[1]);
                team4_diff := rec.Счет5[1] - rec.Счет5[0];
                team4_draw := team4_draw + int4(rec.Счет5[0] = rec.Счет5[1]);
            END IF;

            tmp_team := rec.Матч6[0];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет6[0] > rec.Счет6[1]);
                team1_defeat := team1_defeat + int4(rec.Счет6[0] < rec.Счет6[1]);
                team1_diff := rec.Счет6[1] - rec.Счет6[0];
                team1_draw := team1_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет6[0] > rec.Счет6[1]);
                team2_defeat := team2_defeat + int4(rec.Счет6[0] < rec.Счет6[1]);
                team2_diff := rec.Счет6[1] - rec.Счет6[0];
                team2_draw := team2_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет6[0] > rec.Счет6[1]);
                team3_defeat := team3_defeat + int4(rec.Счет6[0] < rec.Счет6[1]);
                team3_diff := rec.Счет6[1] - rec.Счет6[0];
                team3_draw := team3_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет6[0] > rec.Счет6[1]);
                team4_defeat := team4_defeat + int4(rec.Счет6[0] < rec.Счет6[1]);
                team4_diff := rec.Счет6[1] - rec.Счет6[0];
                team4_draw := team4_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;

            tmp_team := rec.Матч6[1];

            IF rec.Команда1 = tmp_team THEN
                team1_score := team1_score + int4(rec.Счет6[0] < rec.Счет6[1]);
                team1_defeat := team1_defeat + int4(rec.Счет6[0] > rec.Счет6[1]);
                team1_diff := rec.Счет6[1] - rec.Счет6[0];
                team1_draw := team1_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF; 
            IF rec.Команда2 = tmp_team THEN
                team2_score := team2_score + int4(rec.Счет6[0] < rec.Счет6[1]);
                team2_defeat := team2_defeat + int4(rec.Счет6[0] > rec.Счет6[1]);
                team2_diff := rec.Счет6[1] - rec.Счет6[0];
                team2_draw := team2_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;
            IF rec.Команда3 = tmp_team THEN
                team3_score := team3_score + int4(rec.Счет6[0] < rec.Счет6[1]);
                team3_defeat := team3_defeat + int4(rec.Счет6[0] > rec.Счет6[1]);
                team3_diff := rec.Счет6[1] - rec.Счет6[0];
                team3_draw := team3_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;
            IF rec.Команда4 = tmp_team THEN
                team4_score := team4_score + int4(rec.Счет6[0] < rec.Счет6[1]);
                team4_defeat := team4_defeat + int4(rec.Счет6[0] > rec.Счет6[1]);
                team4_diff := rec.Счет6[1] - rec.Счет6[0];
                team4_draw := team4_draw + int4(rec.Счет6[0] = rec.Счет6[1]);
            END IF;

        INSERT INTO Команды VALUES(rec.Команда1, rec.Группа, team1_score, team1_draw, team1_defeat, team1_diff, 3*team1_score+team1_draw);
        INSERT INTO Команды VALUES(rec.Команда2, rec.Группа, team2_score, team2_draw, team2_defeat, team2_diff, 3*team2_score+team2_draw);
        INSERT INTO Команды VALUES(rec.Команда3, rec.Группа, team3_score, team3_draw, team3_defeat, team3_diff, 3*team3_score+team3_draw);
        INSERT INTO Команды VALUES(rec.Команда4, rec.Группа, team4_score, team4_draw, team4_defeat, team4_diff, 3*team4_score+team4_draw);

        team1_score := 0;
        team2_score := 0;
        team3_score := 0;
        team4_score := 0;

        team1_defeat := 0;
        team2_defeat := 0;
        team3_defeat := 0;
        team4_defeat := 0;

        team1_draw := 0;
        team2_draw := 0;
        team3_draw := 0;
        team4_draw := 0;

        team1_diff := 0;
        team2_diff := 0;
        team3_diff := 0;
        team4_diff := 0;

        END LOOP;

    
    END;
$$ LANGUAGE plpgsql;